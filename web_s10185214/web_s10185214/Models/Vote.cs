﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web_s10185214.Models
{
    public class Vote
    {
        public int BookId { get; set; }

        public String Justification { get; set; }
    }
}
